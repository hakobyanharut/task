<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Companies') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="p-3 bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <form action="{{route('company.update', $currentCompany->id)}}" method="post">
                    @csrf
                    @method('put')
                    <div class="grid-cols-2 items-center grid">
                        <div class="m-3">
                            <x-jet-label for="name">Name</x-jet-label>
                        </div>
                        <div class="m-3">
                            <x-jet-input name="name" value="{{$currentCompany->name}}" class="w-full"/>
                            <x-jet-input-error for="name"/>

                        </div>
                        <div class="m-3">
                            <x-jet-label for="parent_id">Parent Company</x-jet-label>
                        </div>
                        <div class="m-3">
                            <select name="parent_company_id" class="form-input w-full rounded-md shadow-sm" id="parent_id">
                                <option value=""></option>
                                @forelse($companies as $company)
                                    <option value="{{$company->id}}" @if($currentCompany->parent_company_id === $company->id) selected @endif>{{$company->name}}</option>
                                @empty
                                @endforelse
                            </select>
                            <x-jet-input-error for="parent_company_id"/>

                        </div>
                    </div>
                    <div class="items-center text-center">
                        <x-jet-button type="submit">
                            Update
                        </x-jet-button>
                    </div>
                </form>


            </div>
        </div>
</x-app-layout>
