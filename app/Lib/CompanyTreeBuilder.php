<?php


namespace App\Lib;


use App\Models\Company;

class CompanyTreeBuilder
{
    private $ids = [];

    public function __construct(Company $company)
    {

        $this->ids[] = $company->id;
        if (isset($company->childCompanies)) {
            $this->iterate($company->childCompanies);
        }


    }

    private function iterate($companies)
    {
        foreach ($companies as $company) {
            if (isset($company->childCompanies)) {
                $this->iterate($company->childCompanies);
            }
            $this->ids[] = $company->id;
        }


    }

    /**
     * @return array
     */
    public function getIds(): array
    {
        return $this->ids;
    }
}
