<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Stations') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div x-data="station()" x-init="list" class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div>
                    <div class="grid-cols-2 items-center grid">
                        <div class="m-3">
                            <x-jet-label for="name">Name</x-jet-label>
                        </div>
                        <div class="m-3">
                            <x-jet-input x-model="name" name="name" class="w-full"/>
                            <p x-show="error.name" x-html="error.name" class="text-sm text-red-600"></p>
                        </div>
                        <div class="m-3">
                            <x-jet-label for="parent_id">Parent Company</x-jet-label>
                        </div>
                        <div class="m-3">
                            <select x-model="company_id" name=company_id"
                                    class="form-input w-full rounded-md shadow-sm"
                                    id="company_id">
                                <option value=""></option>
                                <template x-for="item in companies">
                                    <option :value="item['id']" x-text="item['name']"></option>
                                </template>
                            </select>
                            <p x-show="error.company_id" x-html="error.company_id"
                               class="text-sm text-red-600"></p>
                        </div>
                        <div class="m-3">
                            <x-jet-label for="latitude">Latitude</x-jet-label>
                        </div>
                        <div class="m-3">
                            <x-jet-input x-model="latitude" name="latitude" class="w-full"/>
                            <p x-show="error.latitude" x-html="error.latitude" class="text-sm text-red-600"></p>
                        </div>
                        <div class="m-3">
                            <x-jet-label for="latitude">Longitude</x-jet-label>
                        </div>
                        <div class="m-3">
                            <x-jet-input x-model="longitude" name="latitude" class="w-full"/>
                            <p x-show="error.longitude" x-html="error.longitude" class="text-sm text-red-600"></p>
                        </div>
                    </div>
                    <div class="items-center mb-3 text-center">
                        <x-jet-button x-show="!edit" x-on:click="saveStation" type="submit">
                            Create
                        </x-jet-button>
                        <x-jet-button x-show="edit" x-on:click="updateStation" type="submit">
                            Update
                        </x-jet-button>
                        <x-jet-secondary-button x-show="edit" x-on:click="cancelEdit" type="submit">
                            Cancel Edit
                        </x-jet-secondary-button>
                    </div>
                </div>
                <div class="grid grid-cols-5 gap-2 p-3">
                    <div class="items-center">
                        <label for="filter_distance">Distance</label>
                        <x-jet-input id="filter_distance" type="number" step="1" min="0" x-model="filter_distance"
                                     name="filter_distance" class="w-full"/>
                    </div>
                    <div>
                        <label for="filter_company">Company</label>
                        <select x-model="filter_company" name=filter_company"
                                class="form-input w-full rounded-md shadow-sm"
                                id="filter_company">
                            <option value=""></option>
                            <template x-for="item in companies">
                                <option :value="item['id']" x-text="item['name']"></option>
                            </template>
                        </select>
                    </div>
                    <div>
                        <label for="filter_latitude">Latitude</label>
                        <x-jet-input x-model="filter_latitude" name="filter_latitude" id="filter_latitude"
                                     class="w-full"/>
                    </div>
                    <div>
                        <label for="filter_longitude">Longitude</label>

                        <x-jet-input x-model="filter_longitude" id="filter_longitude" name="filter_longitude"
                                     class="w-full"/>
                    </div>
                    <div class="pt-7">

                        <x-jet-button x-on:click.prevent="filter"> Filter</x-jet-button>
                        <x-jet-secondary-button x-on:click.prevent="resetFilter"> Reset Filter</x-jet-secondary-button>
                    </div>

                </div>
                <template x-for="(item, index) in stations">
                    <div class="grid grid-cols-5 p-3">
                        <div x-text="item['name']"></div>
                        <div x-text="item['company']?item['company']['name']:''"></div>
                        <div x-text="item['longitude']"></div>
                        <div x-text="item['latitude']"></div>
                        <div>
                            <x-jet-button x-on:click.prevent="deleteItem(item['id'])"> Delete</x-jet-button>
                            <x-jet-secondary-button x-on:click.prevent="editItem(index)"> Edit
                            </x-jet-secondary-button>
                        </div>

                    </div>
                </template>
            </div>
        </div>
    </div>
</x-app-layout>

<script>
    function station() {
        return {
            name: '',
            company_id: '',
            error: {},
            edit: false,
            item_id: null,
            latitude: '',
            longitude: '',
            filter_latitude: '',
            filter_longitude: '',
            filter_distance: '',
            filter_company: '',
            companies: [],
            stations: [],
            deleteItem(id) {
                this.error = {};
                axios.delete('/api/station/' + id).then((response) => {
                    this.list()
                });
            },
            resetFilter() {
                this.filter_distance = this.filter_company = this.filter_latitude = this.filter_longitude = '';
                this.listStations();
            },
            filter() {
                axios.get('{{route('station.index')}}', {
                    params: {
                        'company_id': this.filter_company,
                        'distance': this.filter_distance,
                        'longitude': this.filter_longitude,
                        'latitude': this.filter_latitude,
                    }
                }).then((response) => {
                    this.stations = response.data.data;
                });
            },
            editItem(key) {
                this.error = {};

                this.edit = true;
                this.item_id = this.stations[key]['id'];
                this.name = this.stations[key]['name'];
                this.latitude = this.stations[key]['latitude'];
                this.longitude = this.stations[key]['longitude'];
                this.company_id = this.stations[key]['company'] ? this.stations[key]['company']['id'] : '';
            },
            cancelEdit() {
                this.error = {};
                this.edit = false;
                this.name = this.company_id = this.latitude = this.longitude = '';
            },
            list() {
                axios.get('{{route('company.index')}}').then((response) => {
                    this.companies = response.data.data;
                });
                this.listStations();
            },

            listStations() {
                axios.get('{{route('station.index')}}').then((response) => {
                    this.stations = response.data.data;
                });
            },
            updateStation() {
                this.error = {};
                axios.put('/api/station/' + this.item_id, {
                    'name': this.name,
                    'company_id': this.company_id,
                    'latitude': this.latitude,
                    'longitude': this.longitude
                }).then((response) => {
                    this.edit = false;
                    this.name = this.company_id = this.latitude = this.longitude = '';

                    this.listStations();
                }).catch((e) => {
                    let errors = JSON.parse(e.request.response).errors
                    if (errors.name) {
                        this.error.name = errors.name[0];
                    }
                    if (errors.longitude) {
                        this.error.longitude = errors.longitude[0];
                    }
                    if (errors.latitude) {
                        this.error.latitude = errors.latitude[0];
                    }
                    if (errors.company_id) {
                        this.error.company_id = errors.company_id[0];
                    }
                });
            },
            saveStation() {
                this.error = {};
                axios.post('{{route('station.store')}}', {
                    'name': this.name,
                    'company_id': this.company_id,
                    'latitude': this.latitude,
                    'longitude': this.longitude,
                }).then((response) => {
                    this.name = this.company_id = this.latitude = this.longitude = '';
                    this.listStations();
                }).catch((e) => {

                    let errors = JSON.parse(e.request.response).errors
                    if (errors.name) {
                        this.error.name = errors.name[0];
                    }
                    if (errors.longitude) {
                        this.error.longitude = errors.longitude[0];
                    }
                    if (errors.latitude) {
                        this.error.latitude = errors.latitude[0];
                    }
                    if (errors.company_id) {
                        this.error.company_id = errors.company_id[0];
                    }
                });
            }
        }
    }

</script>
