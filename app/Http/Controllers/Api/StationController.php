<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StationRequest;
use App\Http\Resources\StationCollection;
use App\Http\Resources\StationResource;
use App\Models\Station;
use Illuminate\Http\Request;

class StationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return StationCollection
     */
    public function index(Request $request)
    {
        $station = Station::query();
        if (!empty($request->get('company_id'))) {
            $station->getForCompany($request->get('company_id'));
        }
        if (!empty($request->get('latitude')) && !empty($request->get('longitude'))) {
            $distance = 9;
            if (!empty($request->get('distance'))) {
                $distance = $request->get('distance');
            }
            $station->getNearestStations($request->get('latitude'), $request->get('longitude'), $distance);
        }

        return new StationCollection($station->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StationRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StationRequest $request)
    {
        $station = Station::create($request->all());
        return new StationResource($station);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Station $station
     * @return \Illuminate\Http\Response
     */
    public function show(Station $station)
    {
        return new StationResource($station);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Station $station
     * @return \Illuminate\Http\Response
     */
    public function update(StationRequest $request, Station $station)
    {
        $station->update($request->all());
        return new StationResource($station);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Station $station
     * @return \Illuminate\Http\Response
     */
    public function destroy(Station $station)
    {
        $station->delete();
        return response()->json(['msg' => 'Station deleted Successfully']);
    }
}
