<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'parent_company_id'];
    public function getParentKeyName()
    {
        return 'parent_company_id';
    }
    public function companies()
    {
        return $this->hasMany(Company::class, 'parent_company_id');
    }
    public function childCompanies()
    {

        return $this->hasMany(Company::class, 'parent_company_id')->with('childCompanies');
    }


    public function parentCompany()
    {
        return  $this->belongsTo(Company::class, 'parent_company_id');
    }

    public function getRelatedIds()
    {
        $this->load('childCompanies');

        return [5];
    }

}
