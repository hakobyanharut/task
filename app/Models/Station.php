<?php

namespace App\Models;

use App\Lib\CompanyTreeBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Station extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'longitude', 'latitude', 'company_id'];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function scopeGetForCompany($query, $companyId)
    {
        $company = Company::where('id', $companyId)->with('childCompanies')->first();
        if ($company) {
            $companyTree = new CompanyTreeBuilder($company);
            return $query->whereIn('company_id', $companyTree->getIds());
        } else {
            return $query->where('company_id', "undefined");
        }
    }

    public function scopeGetDistance($query, $latitude, $longitude)
    {

        return $query->selectRaw("*,
                     ( 63710 * acos( cos( radians(?) ) *
                       cos( radians( latitude ) )
                       * cos( radians( longitude ) - radians(?)
                       ) + sin( radians(?) ) *
                       sin( radians( latitude ) ) )
                     ) AS distance", [$latitude, $longitude, $latitude]);

    }

    public function scopeGetNearestStations($query, $latitude, $longitude, $radius = 9)
    {
        return $query->getDistance($latitude, $longitude)->having("distance", "<", $radius)
            ->orderBy("distance", 'asc');
    }
}
