<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Companies') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div x-data="company()" x-init="list" class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div>
                    <div class="grid-cols-2 items-center grid">
                        <div class="m-3">
                            <x-jet-label for="name">Name</x-jet-label>
                        </div>
                        <div class="m-3">
                            <x-jet-input x-model="name" name="name" class="w-full"/>
                            <p x-show="error.name" x-html="error.name" class="text-sm text-red-600"></p>

                        </div>
                        <div class="m-3">
                            <x-jet-label for="parent_id">Parent Company</x-jet-label>
                        </div>
                        <div class="m-3">
                            <select x-model="parent_company_id" name="parent_company_id"
                                    class="form-input w-full rounded-md shadow-sm"
                                    id="parent_id">
                                <option value=""></option>
                                <template x-for="item in companies">
                                    <option :value="item['id']" x-text="item['name']"></option>
                                </template>
                            </select>
                            <p x-show="error.parent_company_id" x-html="error.parent_company_id"
                               class="text-sm text-red-600"></p>

                        </div>
                    </div>
                    <div class="items-center mb-3 text-center">
                        <x-jet-button x-show="!edit" x-on:click="saveCompany" type="submit">
                            Create
                        </x-jet-button>
                        <x-jet-button x-show="edit" x-on:click="updateCompany" type="submit">
                            Update
                        </x-jet-button>
                        <x-jet-secondary-button x-show="edit" x-on:click="cancelEdit" type="submit">
                            Cancel Edit
                        </x-jet-secondary-button>
                    </div>
                </div>

                <template x-for="(item, index) in companies">
                    <div class="grid grid-cols-3 p-3">
                        <div x-text="item['name']"></div>
                        <div x-text="item['parent_company']?item['parent_company']['name']:''"></div>
                        <div>
                            <x-jet-button x-on:click.prevent="deleteItem(item['id'])"> Delete</x-jet-button>
                            <x-jet-secondary-button x-on:click.prevent="editItem(index)"> Edit
                            </x-jet-secondary-button>
                        </div>

                    </div>
                </template>
            </div>
        </div>
    </div>
</x-app-layout>

<script>
    function company() {
        return {
            name: '',
            parent_company_id: '',
            error: {},
            edit: false,
            item_id: null,
            companies: [],
            deleteItem(id) {
                this.error =  {};
                axios.delete('/api/company/' + id).then((response) => {
                    this.list()
                });
            },
            editItem(key) {
                this.error =  {};

                this.edit = true;
                this.item_id = this.companies[key]['id'];
                this.name = this.companies[key]['name'];
                this.parent_company_id = this.companies[key]['parent_company'] ? this.companies[key]['parent_company']['id'] : '';
            },
            cancelEdit() {
                this.error =  {};
                this.edit = false;
                this.item_id = '';
                this.name = '';
                this.parent_company_id = '';
            },
            list() {
                axios.get('{{route('company.index')}}').then((response) => {
                    this.companies = response.data.data;
                });
            },
            updateCompany() {
                this.error =  {};
                axios.put('/api/company/'+this.item_id, {
                    'name': this.name,
                    'parent_company_id': this.parent_company_id
                }).then((response) => {
                    this.edit = false;
                    this.item_id = '';
                    this.name = '';
                    this.parent_company_id = '';
                    this.list();
                }).catch((e) => {
                    let errors = JSON.parse(e.request.response).errors
                    if (errors.name) {
                        this.error.name = errors.name[0];
                    }
                    if (errors.parent_company_id) {
                        this.error.parent_company_id = errors.parent_company_id[0];
                    }
                });
            },
            saveCompany() {
                this.error =  {};
                axios.post('{{route('company.store')}}', {
                    'name': this.name,
                    'parent_company_id': this.parent_company_id
                }).then((response) => {
                    this.name = "";
                    this.parent_company_id = '';
                    this.list();
                }).catch((e) => {
                    let errors = JSON.parse(e.request.response).errors
                    if (errors.name) {
                        this.error.name = errors.name[0];
                    }
                    if (errors.parent_company_id) {
                        this.error.parent_company_id = errors.parent_company_id[0];
                    }
                });
            }
        }
    }

</script>
