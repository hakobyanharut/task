<?php

namespace Tests\Feature;

use App\Http\Resources\StationCollection;
use App\Http\Resources\StationResource;
use App\Lib\CompanyTreeBuilder;
use App\Models\Company;
use App\Models\Station;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class StationTest extends TestCase
{
    use RefreshDatabase;

    public $user;
    public $token;

    public function setUp(): void
    {
        parent::setUp();
        $this->user  = User::factory()->create();
        $this->token = $this->user->createToken("test")->plainTextToken;
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateStation()
    {
        $this->actingAs($this->user, 'api');
        $company  = Company::factory()->create();
        $data     = ['company_id' => $company->id, 'latitude' => 44.5590382, 'longitude' => 44.5590382, 'name' => "Babken"];
        $response = $this->post(route('station.store'), $data, ['Authorization' => 'Bearer ' . $this->token]);
        $response->assertStatus(201);
        $this->assertDatabaseHas('stations', $data);
    }

    public function testItReturnAllStations()
    {
        $this->actingAs($this->user, 'api');

        $station  = Station::factory()->count(30)->for(Company::factory())->create();
        $response = $this->getJson(route('station.index'), ['Authorization' => 'Bearer ' . $this->token]);
        $response->assertStatus(200);
        $data     = $response->json('data');
        $stations = StationResource::collection($station)->resolve();

        $this->assertEquals(count($stations), count($data));
    }


    public function testItShouldReturnAllChildStations()
    {

        $company = Company::factory()->has(Company::factory()->has(Company::factory()->has(Company::factory()->has(Company::factory(), 'companies')), 'companies'), 'companies')->create();
        $company->load('childCompanies');

        $companyHelper = new CompanyTreeBuilder($company);
        $ids           = $companyHelper->getIds();
        $stations      = [];
        foreach ($ids as $id) {
            $stations[] = Station::factory(['company_id' => $id])->create();
        }
        $stationList = Station::getForCompany($company->id)->get();

        $this->assertTrue($this->areSimilar($stationList, $stations));

    }

    public function testItShouldCheckDistance()
    {
        $station1 = Station::factory(['latitude' => 40.202161, 'longitude' => 44.553373])->for(Company::factory())->create();
        $station2 = Station::factory(['latitude' => 40.201530, 'longitude' => 44.556141])->for(Company::factory())->create();
        $station3 = Station::factory(['latitude' => -78.994617, 'longitude' => 38.687493])->for(Company::factory())->create();//Antarctica
        $result   = Station::getNearestStations(40.203374, 44.556946, 9)->get();
        $this->assertContains($station1->id, $result->pluck('id'));
        $this->assertContains($station2->id, $result->pluck('id'));
        $this->assertNotContains($station3->id, $result->pluck('id'));

    }

    public function areSimilar($a, $b)
    {

        foreach ($b as $v) {
            if (!count($a->where('id', $v->id))) {
                return false;
            }
        }
        return true;
    }


    public function testItShouldReturnValidStations()
    {
        $this->actingAs($this->user, 'api');
        $company1 = Company::factory()->create();
        $company2 = Company::factory(['parent_company_id' => $company1->id])->create();
        $company3 = Company::factory(['parent_company_id' => $company2->id])->create();
        $station1 = Station::factory(['latitude' => 40.202161, 'longitude' => 44.553373, 'company_id' => $company1->id])->create();
        $station2 = Station::factory(['latitude' => 40.201530, 'longitude' => 44.556141, 'company_id' => $company3->id])->create();
        Station::factory(['latitude' => -78.994617, 'longitude' => 38.687493, 'company_id' => $company2->id])->create();//Antarctica
        $response = $this->getJson(route('station.index', ['company_id' => $company1->id, 'latitude' => 40.203374, 'longitude' => 44.556946, 'distance' => 9]), ['Authorization' => 'Bearer ' . $this->token]);
        $data     = $response->json('data');
        $this->assertSame($data[0]['id'], $station2->id);
        $this->assertSame($data[1]['id'], $station1->id);
        $this->assertTrue(empty($data[2]));

    }

    public function testItShouldUpdateStation()
    {
        $this->actingAs($this->user, 'api');

        $station  = Station::factory()->for(Company::factory())->create();
        $response = $this->putJson(route('station.update', [$station->id]), ['name' => "updated"], ['Authorization' => 'Bearer ' . $this->token]);
        $this->assertDatabaseHas('stations', ['name' => 'updated']);
        $response->assertStatus(200);
        $data = $response->json('data');
        $this->assertEquals('updated', $data['name']);

    }

    public function testItShouldFailValidationForStation()
    {
        $this->actingAs($this->user, 'api');
        $response = $this->postJson(route('station.store'), ['name' => "updated"], ['Authorization' => 'Bearer ' . $this->token]);
        $response->assertStatus(422);


    }

    public function testItShouldDeleteStation()
    {
        $this->actingAs($this->user, 'api');

        $station  = Station::factory()->for(Company::factory())->create();
        $response = $this->deleteJson(route('station.destroy', [$station->id]), [],['Authorization' => 'Bearer ' . $this->token]);
        $this->assertDatabaseMissing('stations', ['id' => $station->id]);
        $response->assertStatus(200);

    }

    public function testItShouldReturnStation()
    {
        $this->actingAs($this->user, 'api');

        $station  = Station::factory()->for(Company::factory())->create();
        $response = $this->getJson(route('station.show', [$station->id]), ['Authorization' => 'Bearer ' . $this->token]);
        $response->assertStatus(200);
        $data = $response->json('data');
        $this->assertEquals($data['name'], $station->name);
    }
}
