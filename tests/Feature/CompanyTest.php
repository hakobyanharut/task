<?php

namespace Tests\Feature;

use App\Lib\CompanyTreeBuilder;
use App\Models\Company;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CompanyTest extends TestCase
{
    use RefreshDatabase;

    public $user;
    public $token;

    public function setUp(): void
    {
        parent::setUp();
        $this->user  = User::factory()->create();
        $this->token = $this->user->createToken("test")->plainTextToken;
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testItShouldCreateCompany()
    {
        $this->actingAs($this->user, 'api');
        $response = $this->postJson(route('company.store'), ['name' => 'Test Name'], ['Authorization' => 'Bearer ' . $this->token]);
        $response->assertStatus(201);
        $this->assertDatabaseHas('companies', ['name' => 'Test Name']);
        $data = $response->json('data');
        $this->assertSame($data['name'], 'Test Name');
    }


    public function testItShouldUpdateCompany()
    {
        $this->actingAs($this->user, 'api');
        $company  = Company::factory()->create();
        $response = $this->putJson(route('company.update', [$company->id]), ['name' => "updated"], ['Authorization' => 'Bearer ' . $this->token]);
        $this->assertDatabaseHas('companies', ['name' => 'updated']);
        $response->assertStatus(200);
        $data = $response->json('data');
        $this->assertSame($data['name'], 'updated');

    }


    public function testItShouldDeleteCompany()
    {
        $this->actingAs($this->user, 'api');
        $company  = Company::factory()->create();
        $response = $this->deleteJson(route('company.destroy', [$company->id]),[], ['Authorization' => 'Bearer ' . $this->token]);
        $this->assertDatabaseMissing('companies', ['id' => $company->id]);
        $response->assertStatus(200);

    }

    public function testItShouldReturnCompany()
    {
        $this->actingAs($this->user, 'api');
        $company  = Company::factory()->create();
        $response = $this->getJson(route('company.show', [$company->id]), ['Authorization' => 'Bearer ' . $this->token]);
        $response->assertStatus(200);
        $data = $response->json('data');
        $this->assertSame($data['name'], $company->name);


    }

    public function testItShouldReturnListAllCompanies()
    {
        $this->actingAs($this->user, 'api');
        $company  = Company::factory()->for(Company::factory(), 'parentCompany')->count(1)->create();
        $response = $this->getJson(route('company.index'), ['Authorization' => 'Bearer ' . $this->token]);
        $response->assertStatus(200);
        $data = $response->json('data');
        $this->assertSame($data[1]['name'], $company[0]->name);


    }

    public function testItWillReturnAllNestedCompanies()
    {
        $this->actingAs($this->user, 'api');

        $company = Company::factory()->has(Company::factory()->has(Company::factory()->has(Company::factory()->has(Company::factory(), 'companies')), 'companies'), 'companies')->create();
        $company->load('childCompanies');
        $companyHelper = new CompanyTreeBuilder($company);
        $ids           = $companyHelper->getIds();
        $lastCompany   = Company::orderBy('id', 'DESC')->first();
        $this->assertContains($lastCompany->id, $ids);

    }


}
