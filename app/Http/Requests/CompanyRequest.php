<?php

namespace App\Http\Requests;

use App\Lib\CompanyTreeBuilder;
use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rul = 'nullable|exists:companies,id';
        if ($this->method() === 'PUT') {
            $tree = new CompanyTreeBuilder($this->route('company')->load('childCompanies'));
            $rul .= '|not_in:' . implode(',', $tree->getIds());
        }

        return [
            'name'              => 'required|max:198',
            'parent_company_id' => $rul,
        ];
    }
}
